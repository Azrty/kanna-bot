Proposed usage:  
(Please use the following guide)
([p] denotes command prefix, [] denotes required arguments, <> denotes optional arguments)  
(Please put intended usages here replacing prefix with [p])
Example: [p]kick [user] [reason]

Possible command aliases:  
(Please put down possible aliases here)
Example: boot, remove, k

Description:  
(Please put a description of new command in here)

Proposed permission level:
(Please Select a permission level from below)
+ User (0)
+ Moderator (2)
+ Administrator (3)
+ Server Owner (4)

Example: Administrator (3)

/assign @martinhyland
/label ~"Type: Feature"
