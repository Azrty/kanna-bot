const {isHex, stripHexPrefix} = require("../util/hexUtils");

exports.run = async  (client, message, [action, ...args]) => {
  if (action !== "set" && action !== "get") return message.reply("You must specify what you want to do!\nDo you want to change role colour or get current one?");
  if (args.length < 1) return message.reply("You should supply some arguments you know? I don't know what to do without them.");
  const func = action === "set" ? changeRoleColour : returnRoleColour;
  message.delete();
  return await func(client, message, args);
};

exports.conf = {
  enabled: true,
  guildOnly: true,
  aliases: ["rolecol","rcol"],
  permLevel: "Administrator"
};

exports.help = {
  name: "rolecolour",
  category: "Moderation",
  description: "Command sets/gets colour for a role.",
  usage: `
    rolecolour [set] [hex value] [role name] - sets role colour
    rolecolour [get] [role name] - returns HEX value`
};

function returnRoleColour(client, message, args) {
  const roleName = args.join(" ");
  const role = message.guild.roles.find(role => role.name === roleName);
  if (!role) return message.reply("This role doesn't exist");
  return message.reply(`Role ${roleName} is colour ${role.hexColor}`);
}
async function changeRoleColour(client, message, args) {
  const settings = message.settings = client.getGuildSettings(message.guild);
  const modlog = message.guild.channels.find(x => x.name === settings.modLogChannel);
  if (!modlog) return message.reply("I cannot find a mod-log channel.");
  const colour = stripHexPrefix(args.shift());
  if (!isHex(colour)) return message.reply("First argument is not hexadecimal number");
  const roleName = args.join(" ");
  const role = message.guild.roles.find(role => role.name === roleName);
  if (!role) return message.reply("This role doesn't exist");
  await role.setColor(colour);

  modlog.send({embed: {
    title: "",
    color: 44678,
    timestamp: Date.now(),
    description: `**Action:** Change role colour\n**Changed Role:** ${roleName}\n**Moderator:** ${message.author.tag}\n**Colour:** ${role.hexColor}`
  }});
}
