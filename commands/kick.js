const Discord = require('discord.js');
exports.run = (client, message, args) => {
  const settings = message.settings = client.getGuildSettings(message.guild);
  const reason = args.slice(1).join(' ');
  const user = message.mentions.users.first();
  const modlog = message.guild.channels.find(x => x.name === settings.modLogChannel);
  if (!modlog) return message.reply('I cannot find a mod-log channel');
  if (reason.length < 1) return message.reply('You must supply a reason for the kick.');
  if (message.mentions.users.size < 1) return message.reply('You must mention someone to kick them.').catch(console.error);

  if (!message.guild.member(user).kickable) return message.reply('I cannot kick that member');
  message.guild.member(user).kick();

  const embed = new Discord.RichEmbed()
  .setColor(0x00AE86)
  .setTimestamp()
  .setDescription(`**Action:** Kick\n**Target:** ${user.tag}\n**Moderator:** ${message.author.tag}\n**Reason:** ${reason}`);
  return client.channels.get(modlog.id).send({embed});
  message.delete();
};

exports.conf = {
  enabled: true,
  guildOnly: true,
  aliases: [],
  permLevel: 'Moderator'
};

exports.help = {
  name: 'kick',
  category: "Moderation",
  description: 'Kicks the mentioned user.',
  usage: 'kick [mention] [reason]'
};
