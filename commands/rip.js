const randommessage = ["You fucked up", "What did you do?", "Don't ever do that again"];

exports.run = (client, message) => {
  if (message.mentions.users.first() === null)
    return message.channel.send("`Error! you did not specify a valid target please try again`");

  if (message.mentions.users.first().bot)
    return message.channel.send("`Error! you did not specify a valid target please try again`");
    
  message.delete();
  message.channel.send({embed: {
    title: "",
    color: 3288894,
    description: `Rip ${message.mentions.users.first()} \n ${randommessage.random()}`
  }});
};

exports.conf = {
  enabled: true,
  guildOnly: true,
  aliases: [],
  permLevel: "User"
};

exports.help = {
  name: "rip",
  category: "Fun",
  description: "Lets you to rip a user",
  usage: "rip [mention]"
};
