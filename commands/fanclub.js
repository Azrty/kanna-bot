exports.run = (client, message, args) => {
  if (message.guild.id == "483565270603137024") {
    let role = message.guild.roles.find(x => x.name === "everyone");
    if (args[0] == "blue") {
      role = message.guild.roles.find(role => role.name === "Ai Hinatsuru Fanclub");
    }
    else if (args[0] == "red") {
      role = message.guild.roles.find(role => role.name === "Ai Yashajin Fanclub");
    }
    else if (args[0] == "neko") {
      role = message.guild.roles.find(role => role.name === "Neko Girl");
    }
    else if (args[0] == "JSK") {
      role = message.guild.roles.find(role => role.name === "JS Kenkyuukai");
    }
    else if (args[0] == "Suku") {
      role = message.guild.roles.find(role => role.name === "Sukumizu");
    }
    else {
      message.channel.send("`Error! You have not specified a valid fanclub`");
    }

    if (message.member.roles.has(role.id)) {
      message.member.removeRole(role);
    }
    else {
      message.member.addRole(role);
    }
    message.delete();
  }
};

exports.conf = {
  enabled: true,
  guildOnly: true,
  aliases: [],
  permLevel: "User"
};

exports.help = {
  name: "fanclub",
  category: "Fun",
  description: "Allows you to join or leave a Fan club. \nCodename: blue (Ai Hinatsuru Fanclub), red (Ai Yashajin Fanclub), neko (Neko Girl), JSK (JS Kenkyuukai), Suku (Sukumizu)",
  usage: "fanclub [codename]"
};
