exports.run = (client, message, args) => {
  const settings = message.settings = client.getGuildSettings(message.guild);
  const reason = args.slice(1).join(' ');
  console.log(reason);
  client.unbanReason = reason;
  client.unbanAuth = message.author;
  const user = args[0];
  const modlog = message.guild.channels.find(x => x.name === settings.modLogChannel);
  if (!modlog) return message.reply('I cannot find a mod-log channel');
  if (reason.length < 1) return message.reply('You must supply a reason for the unban.');
  if (!user) return message.reply('You must supply a User Resolvable, such as a user id.').catch(console.error);
  message.guild.unban(user);
  message.delete();
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: 'Administrator'
};

exports.help = {
  name: 'unban',
  category: 'Moderation',
  description: 'Unbans the user.',
  usage: 'unban [mention] [reason]'
};
