module.exports = async client => {
  // Log that the bot is online.
  client.logger.log(`${client.user.tag}, ready to serve ${client.users.size} users in ${client.guilds.size} servers.`, "ready");
  invite = await client.generateInvite(['ADMINISTRATOR']);
  client.logger.log(`Please use the following link to invite your bot to a guild: ${invite}`);

  // Make the bot "play the game" which is the help command with default prefix.
  client.user.setActivity(`${client.config.defaultSettings.prefix}help`, {type: "LISTENING"});
};
